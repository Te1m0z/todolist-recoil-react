import { Home } from './Pages/index.js';
import { RecoilRoot } from 'recoil';
import './global.css';

function App() {
    return (
        <RecoilRoot>
            <Home />
        </RecoilRoot>
    );
}

export default App;
