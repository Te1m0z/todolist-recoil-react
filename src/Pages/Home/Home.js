import './Home.css';
import { List } from '../../components/index.js';

export function Home() {

    return (
        <>
            <header>
                <h1>To Do List React+Recoil</h1>
            </header>
            <main>
                <div className='home__todototal-container'>
                    <List />
                </div>

                <div className='home__todofilter-container'>
                    {/* <FilterContainer /> */}
                </div>

                {/* <ListContainer /> */}

                <div className='home__todoform-container'>
                    {/* <FormContainer /> */}
                </div>
                
            </main>
        </>
    )
}