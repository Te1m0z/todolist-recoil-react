import { useState } from 'react';
import { useSetRecoilState } from 'recoil';
import { listState } from '../state/index.js';

let id = 0;

function getId() {
  return id++;
}

export function ItemCreator() {
    const [ inputValue, setInputValue ] = useState('');
    const setList = useSetRecoilState(listState);

    const addItem = () => {
        setList(oldList => [ ...oldList, { id: getId(), text: inputValue, isComplete: false }]);
        setInputValue('');
    }

    const onChange = ({target: {value}}) => {
        setInputValue(value)
    }

    return (
        <div>
            <input type='text' value={inputValue} onChange={onChange} />
            <button onClick={addItem}>Add</button>
        </div>
    )
}