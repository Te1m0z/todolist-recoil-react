import { useRecoilValue } from 'recoil';
import { listState } from '../state/index.js';
import { ItemCreator } from './ItemCreator.js';
import { Item } from './Item.js';

export function List() {
    const list = useRecoilValue(listState);

    console.log(list)
    return (
        <>
            <ItemCreator />
            <ul>
                {list.map(item => <Item key={item.key} item={item} />)}
            </ul>
        </>
    )
}