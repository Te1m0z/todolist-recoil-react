import { atom } from 'recoil';

export const listState = atom({
    key: 'listState',
    default: []
});

// export const ItemFilterState = atom({
//     key: 'ItemFilterState',
//     default: 'all'
// });