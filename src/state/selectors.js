// import { selector } from 'recoil';
// import { listState } from './atoms.js';

// export const filteredItemsState = selector({
//     key: 'filteredItemsState',
//     get: ({ get }) => {
//         const filter = get(ItemFilterState);
//         const items = get(ItemState);

//         switch (filter) {
//             case 'Completed':
//                 return items.filter(item => item.isComplete);
//             case 'Uncompleted':
//                 return items.filter(item => !item.isComplete);
//             case 'All':
//             default:
//                 return items;
//         }
//     }
// });

// export const ItemsTotalState = selector({
//     key: 'ItemsTotalState',
//     get: ({ get }) => {
//         const items = get(ItemState);
//         const totalItemsCount = items.length;
//         const totalItemsCompletedCount = items.filter((item) => item.isComplete).length;
//         const totalItemsUncompletedCount = totalItemsCount - totalItemsCompletedCount;
//         const itemsCompletedPercent = Math.round((totalItemsCount === 0 ? 0 : totalItemsCompletedCount / totalItemsCount) * 100);

//         return {
//             totalItemsCount,
//             totalItemsCompletedCount,
//             totalItemsUncompletedCount,
//             itemsCompletedPercent,
//         };
//     },
// });